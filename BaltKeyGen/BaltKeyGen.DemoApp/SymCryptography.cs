using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace BaltKeyGen.DemoApp
{
    public class SymCryptography
    {
        private readonly ServiceProviderEnum _mAlgorithm;
        private readonly SymmetricAlgorithm _mCryptoService;

        public SymCryptography()
        {
            Key = string.Empty;
            Salt = string.Empty;
            _mCryptoService = new RijndaelManaged {Mode = CipherMode.CBC};
            _mAlgorithm = ServiceProviderEnum.Rijndael;
        }

        public SymCryptography(ServiceProviderEnum serviceProvider)
        {
            Key = string.Empty;
            Salt = string.Empty;
            switch (serviceProvider)
            {
                case ServiceProviderEnum.Rijndael:
                    _mCryptoService = new RijndaelManaged();
                    _mAlgorithm = ServiceProviderEnum.Rijndael;
                    break;

                case ServiceProviderEnum.RC2:
                    _mCryptoService = new RC2CryptoServiceProvider();
                    _mAlgorithm = ServiceProviderEnum.RC2;
                    break;

                case ServiceProviderEnum.DES:
                    _mCryptoService = new DESCryptoServiceProvider();
                    _mAlgorithm = ServiceProviderEnum.DES;
                    break;

                case ServiceProviderEnum.TripleDES:
                    _mCryptoService = new TripleDESCryptoServiceProvider();
                    _mAlgorithm = ServiceProviderEnum.TripleDES;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(serviceProvider), serviceProvider, null);
            }
            _mCryptoService.Mode = CipherMode.CBC;
        }

        public SymCryptography(string serviceProviderName)
        {
            Key = string.Empty;
            Salt = string.Empty;

            var str = serviceProviderName.ToLower();
            switch (str)
            {
                case "rijndael":
                    serviceProviderName = "Rijndael";
                    _mAlgorithm = ServiceProviderEnum.Rijndael;
                    break;

                case "rc2":
                    serviceProviderName = "RC2";
                    _mAlgorithm = ServiceProviderEnum.RC2;
                    break;

                case "des":
                    serviceProviderName = "DES";
                    _mAlgorithm = ServiceProviderEnum.DES;
                    break;

                case "tripledes":
                    serviceProviderName = "TripleDES";
                    _mAlgorithm = ServiceProviderEnum.TripleDES;
                    break;
            }

            _mCryptoService = (SymmetricAlgorithm) CryptoConfig.CreateFromName(serviceProviderName);
            _mCryptoService.Mode = CipherMode.CBC;
        }

        public virtual string Decrypt(string cryptoText)
        {
            var buffer = Convert.FromBase64String(cryptoText);
            var legalKey = GetLegalKey();
            _mCryptoService.Key = legalKey;
            SetLegalIV();
            var transform = _mCryptoService.CreateDecryptor();
            try
            {
                var stream = new MemoryStream(buffer, 0, buffer.Length);
                var stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Read);
                var reader = new StreamReader(stream2);
                return reader.ReadToEnd();
            }
            catch
            {
                return null;
            }
        }

        public virtual string Encrypt(string plainText)
        {
            var bytes = Encoding.ASCII.GetBytes(plainText);
            var legalKey = GetLegalKey();
            _mCryptoService.Key = legalKey;
            SetLegalIV();
            var transform = _mCryptoService.CreateEncryptor();
            var stream = new MemoryStream();
            var stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Write);
            stream2.Write(bytes, 0, bytes.Length);
            stream2.FlushFinalBlock();
            var inArray = stream.ToArray();
            return Convert.ToBase64String(inArray, 0, inArray.GetLength(0));
        }

        public virtual byte[] GetLegalKey()
        {
            if (_mCryptoService.LegalKeySizes.Length > 0)
            {
                var num = Key.Length*8;
                var minSize = _mCryptoService.LegalKeySizes[0].MinSize;
                var maxSize = _mCryptoService.LegalKeySizes[0].MaxSize;
                var skipSize = _mCryptoService.LegalKeySizes[0].SkipSize;
                if (num > maxSize)
                {
                    Key = Key.Substring(0, maxSize/8);
                }
                else if (num < maxSize)
                {
                    var num5 = num <= minSize ? minSize : num - num%skipSize + skipSize;
                    if (num < num5)
                    {
                        Key = Key.PadRight(num5/8, '*');
                    }
                }
            }
            var bytes = new PasswordDeriveBytes(Key, Encoding.ASCII.GetBytes(Salt));
            return bytes.GetBytes(Key.Length);
        }

        private void SetLegalIV()
        {
            _mCryptoService.IV = _mAlgorithm == ServiceProviderEnum.Rijndael 
                ? new byte[] {15, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9, 5, 70, 0x9c, 0xea, 0xa8, 0x4b, 0x73, 0xcc} 
                : new byte[] {15, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9};
        }

        public string Key { get; set; }

        public string Salt { get; set; }

        public enum ServiceProviderEnum
        {
            Rijndael,
            RC2,
            DES,
            TripleDES
        }
    }
}

