using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace BaltKeyGen.DemoApp
{
    public class CSecurity
    {
        public string GenProductKey(string userName, string company)
        {
            var st = userName + GetVolumeSerial(Path.GetPathRoot(Application.ExecutablePath)) + Environment.ProcessorCount + company;
            return GetMd5(st);
        }

        public string GetFileData(string text)
        {
            var str4 = new SymCryptography { Key = "fr#$@^^%HJBUYTHAEHJFTRDChjbksfdue37B33&%$#" }.Encrypt(text);
            const string str5 = "^*hdeZEsw5";
            var str2 = "";
            for (var i = 0; i < str4.Length; i++)
            {
                str2 += char.ConvertFromUtf32(Convert.ToInt32(str4[i]) ^ Convert.ToInt32(str5[i]));
            }
            return str2;
        }

        public bool CheckKeys(string userName, string company, string productKey)
        {
            if (!File.Exists(Application.StartupPath + @"\key.gpg"))
                return false;

            var st = userName + GetVolumeSerial(Path.GetPathRoot(Application.ExecutablePath)) + Environment.ProcessorCount + company;
           
            var cryptoText = File.ReadAllText(Application.StartupPath + @"\key.gpg");
            var str4 = new SymCryptography { Key = "fr#$@^^%HJBUYTHAEHJFTRDChjbksfdue37B33&%$#" }.Decrypt(cryptoText);
            const string str5 = "^*hdeZEsw5";
            var str2 = "";
            for (var i = 0; i < str4.Length; i++)
            {
                str2 = str2 + char.ConvertFromUtf32(Convert.ToInt32(str4[i]) ^ Convert.ToInt32(str5[i]));
            }
            DateTime now;
            try
            {
                now = new DateTime(Convert.ToInt32(str2.Split('-')[0]), Convert.ToInt32(str2.Split('-')[1]), Convert.ToInt32(str2.Split('-')[2]));
            }
            catch
            {
                return false;
            }
            return (GetMd5(st) == productKey) && (DateTime.Now < now);
        }

        private static string GetMd5(string st)
        {
            //return FormsAuthentication.HashPasswordForStoringInConfigFile(st, "md5");
            
            var encodedPassword = new UTF8Encoding().GetBytes(st);
            var hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
            var encoded = BitConverter.ToString(hash)
               .Replace("-", string.Empty)
               .ToLower();
            return encoded;
        }

        public string GetUserKey()
        {
            return GetVolumeSerial(Path.GetPathRoot(Environment.CurrentDirectory)) + Environment.ProcessorCount;
        }

        [DllImport("kernel32.dll")]
        private static extern long GetVolumeInformation(string pathName, StringBuilder volumeNameBuffer, uint volumeNameSize, 
            ref uint volumeSerialNumber, ref uint maximumComponentLength, ref uint fileSystemFlags, StringBuilder fileSystemNameBuffer, uint fileSystemNameSize);
        private static string GetVolumeSerial(string strDriveLetter)
        {
            uint volumeSerialNumber = 0;
            uint maximumComponentLength = 0;
            var volumeNameBuffer = new StringBuilder(0x100);
            uint fileSystemFlags = 0;
            var fileSystemNameBuffer = new StringBuilder(0x100);
            GetVolumeInformation(strDriveLetter, volumeNameBuffer, (uint) volumeNameBuffer.Capacity, 
                ref volumeSerialNumber, ref maximumComponentLength, ref fileSystemFlags, fileSystemNameBuffer, (uint) fileSystemNameBuffer.Capacity);
            return Convert.ToString(volumeSerialNumber);
        }

        public bool IsRegistered()
        {
            if (!File.Exists(Application.StartupPath + @"\reg.gpg"))
                return false;
            
            var strArray = File.ReadAllLines(Application.StartupPath + @"\reg.gpg");
            if (strArray.Length < 3)
                return false;

            var userName = strArray[0];
            var company = strArray[1];
            var str3 = strArray[2];
            if ((userName == null) || (company == null) || (str3 == null))
                return false;

            return CheckKeys(userName, company, str3.Replace("-", ""));
        }

        public void WriteRegInfo(string userName, string company, string productKey)
        {
            var contents = new[] { userName, company, productKey };
            File.WriteAllLines(Application.StartupPath + @"\reg.gpg", contents);
        }
    }
}

