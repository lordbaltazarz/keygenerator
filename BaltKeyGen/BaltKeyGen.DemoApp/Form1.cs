﻿using System;
using System.Windows.Forms;

namespace BaltKeyGen.DemoApp
{
    public partial class Form1 : Form
    {
        private readonly CSecurity _security = new CSecurity();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_security.CheckKeys(UserName.Text, Company.Text, ProductKey.Text.Replace("-", "")))
            {
                MessageBox.Show("Ваша версия зарегистрирована. Запустите программу снова...");
                _security.WriteRegInfo(UserName.Text, Company.Text, ProductKey.Text);
                Close();
            }
            else
            {
                MessageBox.Show("Неверный ключ, либо закончен срок действия лицензии! Обратитесь к разработчику");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var productKey = _security.GenProductKey(UserName.Text, Company.Text);
            MessageBox.Show("ProductKey=" + productKey);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var data = _security.GetFileData(Date.Text);
            MessageBox.Show("FileData=" + data);
        }
    }
}
